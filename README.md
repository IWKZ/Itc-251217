### IT Club IWKZ - 25.12.2017

## Requirements
Berikut adalah tools yang sebaiknya sudah terinstall terlebih dahulu sebelum mengikuti Meetup & Live Coding IT Club IWKZ 25.12.2017:

* Visual Studio Code (IDE) => [Download](https://code.visualstudio.com/Download)
* NodeJS => [Download](https://nodejs.org/en/)
* Android SDK (*optional, untuk menjalankan emulator Android) => [Download](https://developer.android.com/studio/index.html) 
* Robomongo => [Download](https://robomongo.org/download)
* Postman => [Download](https://www.getpostman.com/)

### Mobile Apps
Develop mobile apps akan dibuat dengan React Native, sebelumnya pastikan NodeJS sudah terinstall, untuk memastikan NodeJS sudah terinstall silakan jalankan fungsi berikut untuk melihat versi NodeJS:

```bash
$ node -v
```

Setelah itu jalankan fungsi berikut agar dapat membuat project React Native:

```bash
$ npm install -g create-react-native-app
```